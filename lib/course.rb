# encoding: utf-8

class Course
  def initialize (name, cost)
    @name = name
    @cost = cost
  end

  def cost
    return @cost
  end
end

AX = Course.new('App Creator', {1=>1000, 3=>900})
AC = Course.new('App Explorer', {1=>1500, 3=>1300})
AE = Course.new('App Entrepreneur', {1=>2000, 3=>1700})
AL = Course.new('Private Lesson', {1=>3000})
